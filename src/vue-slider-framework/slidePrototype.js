export const slidePrototype = {
  props:{
    init: {type: Object, default: ()=>{return{
      obj: {}
    }}}
  },
  methods: {
    pause(){
      this.$emit('pause');
    },
    unpause(){
      this.$emit('unpause');
    }
  }
};
