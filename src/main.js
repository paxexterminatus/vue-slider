import Vue from 'vue'
import app from './app'

Vue.component('slide-default', require('./vue-slider-framework/slide-default').default);
Vue.component('slide-ticker', require('./vue-slider-framework/slide-ticker').default);

Vue.config.productionTip = false;

new Vue({
  el: '#app',
  components: { app },
  template: '<app/>'
});
